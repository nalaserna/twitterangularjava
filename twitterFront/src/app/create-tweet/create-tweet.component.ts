import { Component, OnInit } from '@angular/core';
import { TweetsService } from '../services/tweets.service';
import { Usuario } from '../model/Usuario';
import { Tweet } from '../model/Tweet';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-tweet',
  templateUrl: './create-tweet.component.html',
  styleUrls: ['./create-tweet.component.css']
})
export class CreateTweetComponent implements OnInit {
  miTweet: Tweet;
  misTweets: Array<Tweet>;
  hoy : Date;
  user : Usuario;

  constructor(private tweetService: TweetsService, private router:Router) { 
    this.miTweet = new Tweet();
    this.hoy = new Date(2013, 9, 22);
    this.user = new Usuario();
    this.user.firstName = '';
    this.user.lastName = '';
    this.user.email = '';
    this.user.password = '';
    this.user.gender = '';
    this.user.birthdate = this.hoy;
    this.user.handle = '';
  }
  

  ngOnInit() {
    let activo = window.localStorage.getItem('handle')
    console.log(activo);

    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
  }

  addnewTweet() {
    this.user.handle = window.localStorage.getItem('handle');
    console.log(this.user.handle);
    this.miTweet.usuario = this.user;
    console.log(this.miTweet.usuario);
    this.tweetService.insertTweet(this.miTweet);
    this.miTweet = new Tweet();

  }

}
