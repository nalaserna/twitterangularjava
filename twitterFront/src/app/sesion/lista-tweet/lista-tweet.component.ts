import { Component, OnInit } from '@angular/core';
import { Tweet } from '../../model/Tweet';
import { TweetsService } from '../../services/tweets.service';
import { Usuario } from 'src/app/model/Usuario';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-lista-tweet',
  templateUrl: './lista-tweet.component.html',
  styleUrls: ['./lista-tweet.component.css']
})
export class ListaTweetComponent implements OnInit {

  miTweet: Tweet;
  misTweets: Array<Tweet>;
  usuarioT : number;
  user : Usuario;
  usua : Observable<Usuario>;
  idT: number;
  textoT: string;

  constructor(private tweetService: TweetsService, private router: Router) {

    this.miTweet = new Tweet();
    this.user = new Usuario();
    tweetService.getAllTweets().subscribe(resp => {
      this.misTweets = resp;
    });
    
  }

  ngOnInit() {
    let activo = window.localStorage.getItem('handle')
    console.log(activo);

    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
  }  

  addnewTweet() {
    console.log('add tweet');
   // this.misTweets.push(tweetAux);
    this.tweetService.insertTweet(this.miTweet);
    this.miTweet = new Tweet();

  }
}
