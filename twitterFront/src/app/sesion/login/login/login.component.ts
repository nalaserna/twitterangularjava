import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TweetsService } from 'src/app/services/tweets.service';
import { Usuario } from 'src/app/model/Usuario';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  handle: string;
  pass: string;
  retorno: Usuario;

  constructor(private tweetService: TweetsService, private router: Router) {
  }

  ngOnInit() {
   
  }



  logIn() {
    this.tweetService.autenticar(this.handle, this.pass).subscribe(text => {
      console.log(text);
      this.retorno = text;
      if (this.retorno.handle != "error"){
        window.localStorage.setItem('handle', this.retorno.handle);
        this.router.navigateByUrl('/home');
      }else{
        alert("Usuario/Contraseña incorrecta")
      }
    });

    
  }

}