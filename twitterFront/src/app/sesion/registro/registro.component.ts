import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/Usuario';
import { Router } from '@angular/router';
import { TweetsService } from 'src/app/services/tweets.service';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usr: Usuario;
  nombre: string;
  firstName: string;
  lastName: string;
  handle: string;
  pwd: string;
  birthdate: Date;
  gender: string;
  email: string;

  constructor(private tweetService: TweetsService, private router: Router ) {
  
  }

  ngOnInit() {
  }

  registerUser() {
    this.usr = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      handle: this. handle,
      gender:this.gender,
      birthdate:this.birthdate,
      password: this.pwd
    };
    console.log('Agregar Usuario ' + this.usr.firstName);
    this.tweetService.registerUser(this.usr);
      this.router.navigateByUrl('/login')
      console.log("Sirvio");
  }
}