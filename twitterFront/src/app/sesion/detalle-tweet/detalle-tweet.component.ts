import { Component, OnInit, Input } from '@angular/core';
import { Tweet } from '../../model/Tweet';
import { Usuario } from 'src/app/model/Usuario';

@Component({
  selector: 'app-detalle-tweet',
  templateUrl: './detalle-tweet.component.html',
  styleUrls: ['./detalle-tweet.component.css']
})
export class DetalleTweetComponent implements OnInit {

  constructor() {
    
    
    /*this.mySelectedTweet = {
      id: 2,
      usuario: this.user,
      texto: 'Mi primer tweet',
      fecha: new Date(3/21/2019)
    };*/
   }
   

  @Input() mySelectedTweet: Tweet;
  @Input() user: Usuario;
  ngOnInit() {
  }


}
