import { Component, OnInit, Input } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { TweetsService } from '../services/tweets.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Tweet } from '../model/Tweet';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  [x: string]: any;

  constructor(api: TweetsService, private route: ActivatedRoute, private router : Router) {
    this.api = api;
    this.userid = this.route.snapshot.params['id'];
    this.api.viewUser(this.userid).subscribe(resp => {
      console.log('Viendo usuario ID: '+this.userid);
      this.user = resp;
    });

    this.api.getTweetsByUser(this.userid).subscribe(resp => {
      this.misTweets = resp;
    });
   }
   api: TweetsService;
   @Input() mySelectedTweet: Tweet;
   @Input() user: Usuario;
   @Input() userid: number;
   @Input() misTweets: Array<Tweet>;

  ngOnInit() {
    let activo = window.localStorage.getItem('handle')
    console.log(activo);

    if (activo == null) {
      this.router.navigateByUrl('/login');
    }
    
   
    //this.api.viewUser(this.userid).subscribe((user) => { console.log('User:', user); this.user = user; });
  }

}
