import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TweetsComponent } from './tweets/tweets.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ListaTweetComponent } from './sesion/lista-tweet/lista-tweet.component';
import { DetalleTweetComponent } from './sesion/detalle-tweet/detalle-tweet.component';
import { CreateTweetComponent } from './create-tweet/create-tweet.component';
import { RegistroComponent } from './sesion/registro/registro.component';
import { LoginComponent } from './sesion/login/login/login.component';


const routes: Routes = [
  {path:'user-profile/:id', component:UserProfileComponent},
  {path: 'home', component: ListaTweetComponent },
  {path:'new-tweet', component: CreateTweetComponent},
  {path:'register', component: RegistroComponent},
  {path:'login', component: LoginComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home' }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
