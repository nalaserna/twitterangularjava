import { Component, OnInit, Input } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { TweetsService } from '../services/tweets.service';

@Component({
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css']
})
export class TweetsComponent implements OnInit {

  constructor(api: TweetsService) {

    this.api = api;
   /* this.user = {
      
      id: 1,
      firstName: 'Natalia',
      lastName: 'Laserna',
      email: 'nalaserna@poligran.edu.co',
      gender: 'femenino',
      birthdate: new Date(1989, 9, 5),
      handle: 'nalaserna',
      password: '123123'

    };*/

  }

  @Input() user: Usuario;
  api: TweetsService;

  ngOnInit() {
    this.api.getUsuarioByHandle('LordMalfoy').subscribe((user) => { console.log('User:', user); this.user = user; });
  }
}


