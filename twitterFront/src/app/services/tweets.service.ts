import { Injectable } from '@angular/core';
import { Tweet } from '../model/Tweet';
import { Usuario } from '../model/Usuario';
import { ALL_TWEETS } from '../datos/datos';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TweetsService {
  [x: string]: any;

  constructor(private http: HttpClient) { }

  public getAllTweets(): Observable<Tweet[]> {
    return this.http.get<Tweet[]>(environment.urlConsultaTweets);
  }
  
  public autenticar(handle: string, password:string){
    const body = new HttpParams().set('handle', handle+'').set('password', password+'');
    return this.http.post<Usuario>(environment.urlLogin, body);
  }

  public registerUser(usuario: Usuario) {
    const body = new HttpParams().set('firstName', usuario.firstName + '')
      .set('lastName', usuario.lastName + '')
      .set('fecha_nacimiento', usuario.birthdate + '')
      .set('gender', usuario.gender + '')
      .set('email', usuario.email)
      .set('password', usuario.password)
      .set('handle', usuario.handle);
    return this.http.post(environment.urlRegister, body).subscribe();
}

  public insertTweet(tweet:Tweet){

    console.log("EL TWEET A INSERTAR:", tweet);
    const body = new HttpParams().set('handle', tweet.usuario.handle+'').set('texto', tweet.texto);

    return this.http.post(environment.urlInsertaTweets, body).subscribe();
  }

  public viewUser(userid: number): Observable<Usuario>{
    const options = { params: new HttpParams().set('id', userid+'')};
    return this.http.get<Usuario>(environment.urlViewUser, options);
    console.log("Sirvio");
  }

  public getUsuarioByHandle(handle: string): Observable<Usuario>{
    console.log("El handle es: ", handle);
    const options = { params: new HttpParams().set('handle', handle+'') };

    return this.http.get<Usuario>(environment.urlConsultarUsuarioByHandle, options);
  }

  public getTweetsByUser(userid: number): Observable<Tweet[]>{
    console.log("El id es: ", userid);
    const options = { params: new HttpParams().set('id', userid+'') };

    return this.http.get<Tweet[]>(environment.urlConsultarTweetsByUser, options);
  }
}

