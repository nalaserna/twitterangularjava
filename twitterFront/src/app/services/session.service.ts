import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/model/Usuario';

@Injectable()
export class SessionService {

  private isUserLoggedIn;
  public usserLogged: string;

  constructor() { 
  	this.isUserLoggedIn = false;
  }

  setUserLoggedIn(user: string) {
    this.isUserLoggedIn = true;
    this.usserLogged = user;
    window.localStorage.setItem('currentUser', user); // ok 
    console.log("Sesion creada");
    console.log(user);
    console.log(this.usserLogged);
  
  }

  getUserLoggedIn() {
  	window.localStorage.getItem('currentUser');
  }

}