import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'twitterSample';


  constructor(translate: TranslateService, private router: Router) {
    translate.setDefaultLang('en');
    let browserLang = translate.getBrowserLang();
    translate.use(browserLang);    
}

public signOut() {
  window.localStorage.removeItem('handle');
  this.router.navigateByUrl('/login');
}
}
