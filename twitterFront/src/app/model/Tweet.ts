import { Usuario } from './Usuario';

export class Tweet{

    id: number;
    texto: string;
    usuario: Usuario;
    fecha: Date;
}
