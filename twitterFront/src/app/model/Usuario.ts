export class Usuario{
    id:number;
    firstName: string;
    lastName: string;
    email: string;
    gender: string;
    birthdate: Date;
    password: string;
    handle: string;
}
