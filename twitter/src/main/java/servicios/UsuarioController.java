package servicios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Usuario;
import repositories.UsuarioRepository;

@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	
	@RequestMapping(path="/getAllUsers", method=RequestMethod.GET)
	public Iterable<Usuario> getAllUsers () {
		
		Iterable<Usuario> findAll = usuarioRepositoryDAO.findAll();
		return findAll;
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/addUser", method=RequestMethod.POST) 
	public @ResponseBody String addNewUser
	(@RequestParam String firstName, @RequestParam String lastName, 
	@RequestParam String email, @RequestParam String gender, 
	@RequestParam String password, @RequestParam String fecha_nacimiento,
	@RequestParam String handle) throws ParseException {
		
		Date birthdate = new SimpleDateFormat("yyyy-MM-dd").parse(fecha_nacimiento);  
		
		Usuario nuevoUsuario = new Usuario();
		nuevoUsuario.setFirstName(firstName);
		nuevoUsuario.setLastName(lastName);
		nuevoUsuario.setEmail(email);
		nuevoUsuario.setGender(gender);
		nuevoUsuario.setHandle(handle);
		nuevoUsuario.setPassword(password);
		nuevoUsuario.setBirthdate(birthdate);
	
		usuarioRepositoryDAO.save(nuevoUsuario);
		return "Usuario Guardado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public @ResponseBody Usuario login(@RequestParam String handle, @RequestParam String password)
			throws ParseException {
		
		Date hoy = new Date();

		Usuario usuario = usuarioRepositoryDAO.findByHandle(handle);
		Usuario retorno = new Usuario();
		retorno.setFirstName("error");
		retorno.setLastName("error");
		retorno.setEmail("error");
		retorno.setHandle("error");
		retorno.setBirthdate(hoy);
		retorno.setGender("error");
		retorno.setPassword("error");
		if (usuario != null) {
			if (usuario.getPassword().equals(password) == true) {
				retorno = usuario;
			} 
		} 
			return retorno;
	}


	
	@RequestMapping(path="/deleteUser", method=RequestMethod.POST) 
	public @ResponseBody String deleteUser
	(@RequestParam String id) {
		
		int idusuario = Integer.parseInt(id);
		
		Usuario usuario = usuarioRepositoryDAO.findById(idusuario);
		usuarioRepositoryDAO.delete(usuario);
		return "Usuario Eliminado";
		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getUserById", method=RequestMethod.GET)
	public Usuario getUserById (@RequestParam int id) {
		//int idusuario = Integer.parseInt(id);
		//System.out.println(idusuario);
		
		return usuarioRepositoryDAO.findById(id);
	}
	
	@RequestMapping (path="/getUserByEmail", method=RequestMethod.GET)
	public Usuario getUserByEmail (@RequestParam String email) {

		return usuarioRepositoryDAO.findByEmail(email);
	
	}
	
	@CrossOrigin
	@RequestMapping (path="/getUserByHandle", method=RequestMethod.GET)
	public Usuario getUserByHandle (@RequestParam String handle) {

		return usuarioRepositoryDAO.findByHandle(handle);
	
	}
	
}
